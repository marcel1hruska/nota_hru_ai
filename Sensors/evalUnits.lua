local sensorInfo = {
	name = "evalUnits",
	desc = "return units seeking for hills",
	author = "Marcel",
	date = "2019",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local selectedUnits = Spring.GetSelectedUnits()
local minX = 0

function getHighGround(unitID)
	toReturn = { pos = Vec3(0,0,0), move = false }
    if (unitID == nil) then
        return toReturn
	end
	local x,y,z = Spring.GetUnitPosition(unitID)
	x = x + 100
    for i = -100, 100,10
	do
		height = Spring.GetGroundHeight(x, z + i)
		if height > y + 10 and x > minX then 
			if i < 0 then
				i = i - 20
			else
				i = i + 20
			end
			while height > y and x > minX do
				toReturn = { pos = Vec3(x, height, z + i), move = true }
				x = x + 20
				y = height
				height = Spring.GetGroundHeight(x, z + i)
			end
			-- make minX
			while height == y do
				x = x + 20
				y = height
				height = Spring.GetGroundHeight(x, z + i)
			end
			minX = x
			break
		end
    end
    return toReturn
end

function filter(elements, predicate)
    local result = {}
    for _, element in ipairs(elements) do
        if predicate(element) then result[#result] = element end
    end
    return result
end

-- @description return current all seeking units
return function(uts)
	local returnTable = {}
	if uts == nil then
		returnTable = { 
			units = {}, 
			guard = nil, 
			guardPos = nil
		}
	else
		returnTable = { 
			units = {}, 
			guard = uts.guard, 
			guardPos = uts.guardPos
		}
	end

	if guard ~= nil then
		x,y,z = Spring.GetUnitPosition(guard)
		--Spring.Echo(x)
		if x == guardPos.x and y == guardPos.y and z == guardPos.z then
			guard = nil
			guardPos = nil
		end
	end

	local guardFound = false
	local j = 1
	local toRemove = 0
	for i = 1, #selectedUnits
	do
		hg = getHighGround(selectedUnits[i])
		if hg.move and guardFound == false then
			Spring.GiveOrderToUnit(selectedUnits[i], CMD.MOVE, hg.pos:AsSpringVector(), {})
			returnTable.guard = selectedUnits[i]
			toRemove = i
			returnTable.guardPos = hg.pos
			guardFound = true
		else
			returnTable.units[selectedUnits[i]] = j
			--local x,y,z = Spring.GetUnitPosition(table[i])
			--returnTable.guardPos[j] = {Spring.GetGroundHeight(x + 100, z + 100),y}
			j = j + 1
		end
	end

	if toRemove ~= 0 then
		table.remove(selectedUnits,toRemove)
	end

	return returnTable
end