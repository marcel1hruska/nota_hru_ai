local sensorInfo = {
	name = "getSavior",
	desc = "get free atlas",
	author = "Marcel",
	date = "2019",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

--speed-ups
local myID = Spring.GetMyAllyTeamID()
local teamUnits = Spring.GetTeamUnits
local unitDefId = Spring.GetUnitDefID
local getPosition = Spring.GetUnitPosition

-- smallest distance
function closest(unit, units)
    unitx,unity,unitz = getPosition(unit)
    local closest = { id = -1, pos = Vec3(0,0,0), dist = Game.mapSizeZ }
    for i = 1, #units do
        currx,curry,currz = getPosition(units[i])
        xdiff = currx-unitx
        zdiff = currz-unitz
        distance = math.sqrt(xdiff*xdiff + zdiff*zdiff)
        if distance < closest["dist"] then
            closest = { index = i, pos = Vec3(currx,curry,currz), dist = distance, id = units[i] }
        end
    end
    return closest
end

-- find free unit in list
function findFree(units)
    for id,bool in pairs(units) do
        if bool == false then
            return id
        end
    end
    return nil
end

-- check for empty
function isEmpty(array)
    if array == nil then
        return true
    end
    for _,_ in pairs(array) do
        return false
    end
    return true
end


-- @description return all found units
return function()
    -- find units to save
    local myTeamUnits = teamUnits(myID)
    -- list of atlas -> list of targets
    local couples = {}
    local toSave = {}
    local atlases = {}

    -- initialize toSave with all possible targets and atlases
    for i = 1, #myTeamUnits
    do
        if UnitDefs[unitDefId(myTeamUnits[i])].name ~= "armpeep" and
            UnitDefs[unitDefId(myTeamUnits[i])].name ~= "armatlas" and
            UnitDefs[unitDefId(myTeamUnits[i])].name ~= "armrad" and
            UnitDefs[unitDefId(myTeamUnits[i])].name ~= "armwin" then
                toSave[#toSave + 1] = myTeamUnits[i]
        end
        if UnitDefs[unitDefId(myTeamUnits[i])].name == "armatlas" then
            atlases[#atlases + 1] = myTeamUnits[i]
        end
    end

    -- match atlases with their closes targets to save
    while isEmpty(toSave) == false do
        for i = 1, #atlases do 
            if isEmpty(toSave) then
                break
            end
            target = closest(atlases[i],toSave)
            table.remove(toSave,target["index"])
            if (isEmpty(couples[i])) then
                couples[i] = { savior = atlases[i], targets = {target} }
            else
                table.insert(couples[i]["targets"],target)
            end
        end
    end

    return couples
end