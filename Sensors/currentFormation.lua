local sensorInfo = {
	name = "currentFormation",
	desc = "create formation for currently selected units",
	author = "Marcel",
	date = "2019",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnits = Spring.GetSelectedUnits

-- @description return formation of selected units
return function()
    local uts = SpringGetUnits()
	local returnTable = {}
	for i = 1, #units
	do
		returnTable[i] = Vec3(0,0,Game.mapSizeZ/#uts*i)
	end
	return returnTable
end