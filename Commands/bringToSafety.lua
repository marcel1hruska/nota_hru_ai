function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Save defined units while avoiding enemies",
		parameterDefs = {
            { 
				name = "savior",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            },
			{ 
				name = "targets",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            },
			{ 
				name = "safeArea",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- check for empty
function isEmpty(array)
    if array == nil then
        return true
    end
    for _,_ in pairs(array) do
        return false
    end
    return true
end


-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local getUnitPos = Spring.GetUnitPosition
local unitDefId = Spring.GetUnitDefID
local isdead = Spring.GetUnitIsDead
function targetIsSafe(id,area)
    unitx,unity,unitz = getUnitPos(id)
    if unitx == nil then
        return true
    end
    xdiff = area[1]-unitx
    zdiff = area[3]-unitz
    distance = math.sqrt(xdiff*xdiff + zdiff*zdiff)
    if distance <= area[4] then
        return true
    end
    return false
end

function Run(self, units, parameter)
    local safeArea = { parameter.safeArea["center"]["x"],parameter.safeArea["center"]["y"],parameter.safeArea["center"]["z"],parameter.safeArea["radius"] }
    local id = parameter.savior
    local targets = parameter.targets

    atlasPosX,atlasPosY,atlasPosZ = getUnitPos(id)
    if isEmpty(Spring.GetUnitCommands(id,1)) then
        for i = #targets, 1,-1 do 
            if (targetIsSafe(targets[i]["id"],safeArea) == false) then
                SpringGiveOrderToUnit(id, CMD.MOVE, Vec3(targets[i]["pos"]["x"],atlasPosY,atlasPosZ):AsSpringVector(), {})
                SpringGiveOrderToUnit(id, CMD.MOVE, Vec3(targets[i]["pos"]["x"],atlasPosY,atlasPosZ):AsSpringVector(), {"shift"})
                SpringGiveOrderToUnit(id, CMD.MOVE, targets[i]["pos"]:AsSpringVector(), {"shift"})
                SpringGiveOrderToUnit(id, CMD.LOAD_UNITS, {targets[i]["id"]}, {"shift"})
                SpringGiveOrderToUnit(id, CMD.MOVE, Vec3(targets[i]["pos"]["x"],atlasPosY,parameter.safeArea["center"]["z"]):AsSpringVector(), {"shift"})
                SpringGiveOrderToUnit(id, CMD.UNLOAD_UNITS, safeArea, {"shift"})
                SpringGiveOrderToUnit(id, CMD.MOVE, Vec3(atlasPosX,atlasPosY,atlasPosZ):AsSpringVector(), {"shift"})
            end
        end 
    end

    return RUNNING
end